Web sémantique et données ouvertes
==================================

Nous innovons avec CubicWeb, une plate-forme libre de développement pour le web sémantique et les données ouvertes. CubicWeb a reçu le prix DataConnexions de la mission Etalab pour l'Open Data et a servi de base pour de nombreux catalogues d'institutions culturelles. Ces accomplissements sont le fruit d'années de R&D et d'expérience dans la modélisation, l'écharge et l'analyse de données.