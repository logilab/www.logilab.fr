template_rest = u'''
{title}
{underline}


:slug: {slug}
:date: {creation_date}
:tags: {etype}
:category: {category}

{content}

'''

template_html = u'''
<html>
    <head>
        <title>{title}</title>
        <meta name="slug" content="{slug}" />
        <meta name="date" content="{creation_date}" />
        <meta name="tags" content="{etype}" />
        <meta name="category" content="{category}" />
    </head>
    <body>
{content}
    </body>
</html>

'''

templates = {
    u'text/html': template_html,
    u'text/rest': template_rest
}

article_path = 'all_articles/{}/{}-{}.{}'
redirect_string_one = "Redirect /{etype}/{eid} /{year}/{month}/{slug}\n"
redirect_string_two = "Redirect /{eid} /{year}/{month}/{slug}\n"
