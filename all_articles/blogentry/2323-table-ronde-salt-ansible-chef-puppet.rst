
Table ronde Salt - Ansible - Chef - Puppet
##########################################


:slug: table-ronde-salt-ansible-chef-puppet
:date: 2015/07/24 11:23:08
:tags: BlogEntry

Logilab a organisé une table ronde pour comparer les outils libres de gestion d'infrastructure et de gestion de configuration système : `Salt, Ansible, Chef et Puppet <http://salt-fr.afpy.org/compte-rendu-table-ronde-et-comparaison-des-frameworks-salt-ansible-chef-puppet.html>`_.

Elle a eu lieu le 18 juin 2015 à l'Ecole42 à Paris.

