
Rencontres Régionales du Logiciel Libre 2017
############################################


:slug: rencontres-regionales-du-logiciel-libre-2017
:date: 2017/11/20 16:44:27
:tags: BlogEntry

.. image :: https://www.logilab.fr/file/2757/raw

Pour la 5ème édition de ce rendez-vous, les Rencontres Régionales du Logiciel Libre s'installent cette année à l'Hippodrome de Toulouse, et pour la toute première fois à Montpellier.

Ces rencontres s'adressent aussi bien aux services informatiques qu'aux directions métiers qui trouveront des réponses à leurs problématiques techniques et besoins fonctionnels.

Les RRLL sont ainsi l'occasion de diverses rencontres telles que des administrations, collectivités, industries et entreprises ayant déployé des solutions libres, ainsi que les prestataires locaux.
Les Rencontres Régionales du Logiciel Libre sont une série d'évènements dans toute la France organisés sous l'égide du `Conseil National du Logiciel Libre (CNLL) <http://www.cnll.fr>`_. 

Les RRLL de Toulouse sont inscrites dans le cadre de la manifestation `Capitole du Libre <https://2017.capitoledulibre.org>`_ organisée tous les ans par l' `Association Toulibre <http://www.toulibre.org/>`_.

**À cette occasion, Logilab présentera "Tirer parti du Web des données pour améliorer l'efficacité des administrations et des entreprises".** 

Consultez les programmes :

- À `Toulouse <http://www.solibre.fr/fr/actualites/edition-2017-des-rrll.html>`_ mardi 21 novembre

- À `Montpellier <http://www.solibre.fr/fr/actualites/edition-2017-des-rrll-montpellier.html>`_ jeudi 23 novembre

