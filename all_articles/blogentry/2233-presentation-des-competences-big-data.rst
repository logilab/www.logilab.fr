
Présentation des compétences Big Data
#####################################


:slug: presentation-des-competences-big-data
:date: 2015/01/29 16:15:48
:tags: BlogEntry

Ce matin, lors de la plénière Big Data du Groupe Thématique "Outils de Conception et de Développement de Systèmes" du pôle Systematic Paris Région, Olivier Cayrol, Directeur Adjoint de Logilab, a évoqué nos compétences dans ce domaine.

Découvrez la présentation (courte) de  Logilab sur slideshare :
http://www.slideshare.net/logilab/competences-logilab-systematicocds

