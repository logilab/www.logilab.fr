
Objectifs de Logilab en 2014
############################


:slug: objectifs-de-logilab-en-2014
:date: 2014/01/22 15:16:23
:tags: BlogEntry

En 2013, Logilab a atteint les objectifs de son plan de développement en poursuivant sa croissance dans un contexte économique difficile:

- l'`établissement de Toulouse <https://maps.google.fr/?q=logilab+toulouse>`_ a été ouvert ;

- l'application `Simulagora <http://www.simulagora.com>`_ a été déployée ;

- la plate-forme `CubicWeb <http://www.cubicweb.org>`_ a progressé dans plusieurs domaines: IHM et visualisation (bootstrap, d3js, webgl) ; manipulation de grandes quantités de données (dizaines de millions d'objets) ; élargissement de la communauté (France, Mexique, Belgique, etc).

En 2014, Logilab:

- continuera à soutenir la croissance de CubicWeb ;

- commercialisera Simulagora ;

- appronfondira sa maîtrise de Javascript pour proposer des IHM web dont l'interactivité n'aura rien à envier aux applications bâties sur Qt/Gtk/OpenGL ;

- contribuera à la diffusion de `Salt <https://fr.wikipedia.org/wiki/Salt_%28logiciel%29>`_ en France et en Europe.

