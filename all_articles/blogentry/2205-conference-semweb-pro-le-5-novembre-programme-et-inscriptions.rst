
Conférence SemWeb.Pro le 5 novembre, programme et inscriptions
##############################################################


:slug: conference-semweb-pro-le-5-novembre-programme-et-inscriptions
:date: 2014/09/22 15:53:18
:tags: BlogEntry

La conférence SemWeb.Pro est de retour le 5 novembre 2014 avec un `programme et des intervenants
de grande qualité <http://www.semweb.pro/semwebpro-2014.html#programme>`_. Cette fois encore, ce 
sera l'occasion de découvrir de nouvelles utilisations du Web Sémantique dans le mode professionnel.
Les novices apprendront et pourront échanger avec les experts scientifiques et industriels.

`Réservez vos places <http://www.eventbrite.fr/e/billets-semwebpro-journee-de-conference-pour-les-professionnels-du-web-semantique-13174251541>`_ dès à présent !

.. image :: http://www.semweb.pro/file/3343/raw/semwebpro2014.png

