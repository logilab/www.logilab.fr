
Logilab à l'OpenWorldForum 2013
###############################


:slug: logilab-a-l-openworldforum-2013
:date: 2013/09/25 16:38:58
:tags: BlogEntry

Rendez-nous visite sur notre stand lors de l'`OpenWorldForum <http://openworldforum.org/>`_ à Paris (Montrouge) le 3 et 4 octobre 2013. Nous pourrons parler de vos projets ou de nos dernières réalisations. Nous organisons un `meetup SaltStack <http://www.logilab.org/179215>`_ et aborderons plus généralement l'application du `TDD (Test-driven development) <https://fr.wikipedia.org/wiki/Test-driven_development>`_ appliqué à l'administration système.


.. image :: http://openworldforum.org/static/pictures/Calque1.png
  :align: center

