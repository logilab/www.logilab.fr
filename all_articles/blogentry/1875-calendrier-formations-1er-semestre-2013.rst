
Calendrier formations 1er semestre 2013
#######################################


:slug: calendrier-formations-1er-semestre-2013
:date: 2012/11/28 10:40:49
:tags: BlogEntry

Le calendrier_ des formation inter-entreprises pour le 1er semestre 2013 est disponible. Inscrivez-vous aux prochaines sessions "Programmation objet avec Python", "Programmation Python avancée", "Python pour le scientifique" et "Créer des interfaces graphiques avec Python et Qt".

.. _calendrier: https://www.logilab.fr/formations

