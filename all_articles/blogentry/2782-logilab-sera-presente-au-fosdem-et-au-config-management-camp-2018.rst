
Logilab sera présente au FOSDEM et au Config Management Camp 2018
#################################################################


:slug: logilab-sera-presente-au-fosdem-et-au-config-management-camp-2018
:date: 2018/02/01 14:19:34
:tags: BlogEntry

FOSDEM est une conférence qui réunit chaque année des milliers de développeurs de logiciels libres et open source du monde entier à Bruxelles.

Cette année ce rendez-vous incontournable aura lieu samedi 3 et dimanche 4 février à l'Université Libre de Bruxelles (ULB Solbosch Campus).

.. image:: https://www.logilab.fr/file/2783/raw


Logilab participera ensuite au `Config Management Camp <http://cfgmgmtcamp.eu/>`_ qui aura lieu du lundi 5 au mercredi 7 février à Gent.



.. image:: https://www.logilab.fr/file/2784/raw

Nous vous donnons rendez-vous mardi 6 février dans la **Community Room Salt B.3.036**.

- À partir de 15h40, Arthur présentera `Use SaltStack to deploy a full monitoring and supervision stack <http://cfgmgmtcamp.eu/schedule/salt/monitoring.html>`_.

- Et à partir de 16h20, Philippe présentera `Use testinfra to test your infrastructure deployed with SaltStack <http://cfgmgmtcamp.eu/schedule/salt/test.html>`_.
 

`Consultez le programme <http://cfgmgmtcamp.eu/schedule/index.html>`_ et **rencontrons-nous sur place !**

Suivez nos actualités sur ce blog ou sur Twitter :

- `@logilab <https://twitter.com/logilab>`_

- `@arthurlutz <https://twitter.com/arthurlutz>`_

- `@douardda <https://twitter.com/douardda>`_

- `@philpep_ <https://twitter.com/philpep_>`_

