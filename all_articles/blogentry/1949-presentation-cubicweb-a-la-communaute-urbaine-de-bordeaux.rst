
Présentation CubicWeb à la communauté urbaine de Bordeaux
#########################################################


:slug: presentation-cubicweb-a-la-communaute-urbaine-de-bordeaux
:date: 2013/11/26 15:44:58
:tags: BlogEntry

C'est avec grand plaisir que nous répondons présent à l'invitation de Pascal Romain, responsable OpenData du Conseil Général de la Gironde, à venir présenter CubicWeb_ lors d'un atelier "Web des données".

Celui-ci aura lieu lundi 2 décembre de 16h à 20h au Node_ à Bordeaux. Programme détaillé et inscription ici_.


.. image :: http://www.datalocale.fr/drupal7/sites/all/themes/cg33/logo.png
   :align: center
.. image :: http://www.logilab.fr/file/1398/raw/banner3.png
  :align: center

.. _CubicWeb: http://www.cubicweb.org
.. _Node: http://bxno.de/
.. _ici: http://www.datalocale.fr/drupal7/S03E02-web2donnees

