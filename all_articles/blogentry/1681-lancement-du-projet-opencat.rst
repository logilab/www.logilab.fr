
Lancement du projet OpenCat
###########################


:slug: lancement-du-projet-opencat
:date: 2012/03/29 20:42:01
:tags: BlogEntry

Le projet OpenCat vient d'être sélectionné_ par le Ministère de la Culture. Ce projet mettra les données ouvertes de la Bibliothèque nationale de France (`data.bnf.fr`_) au service des bibliothèques publiques en utilisant des logiciels libres, dont CubicWeb_.

.. _sélectionné: http://www.culturecommunication.gouv.fr/Espace-Presse/Communiques/Appel-a-projets-2012-Services-numeriques-culturels-innovants-60-projets-pour-developper-de-nouveaux-usages-numeriques-culturels
.. _CubicWeb: http://www.cubicweb.org
.. _`data.bnf.fr`: http://data.bnf.fr

