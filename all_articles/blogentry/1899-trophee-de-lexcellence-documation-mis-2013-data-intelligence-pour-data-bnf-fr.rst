
Trophée de l’Excellence Documation - MIS 2013 « Data Intelligence » pour data.bnf.fr
####################################################################################


:slug: trophee-de-lexcellence-documation-mis-2013-data-intelligence-pour-data-bnf-fr
:date: 2013/03/21 14:59:03
:tags: BlogEntry

`data.bnf.fr <http://data.bnf.fr>`_, réalisé par Logilab en s'appuyant sur le logiciel libre CubicWeb, a reçu aujourd'hui le `Trophée de l'Excellence « Data Intelligence » <http://www.veillemag.com/Nomines-Laureats-et-Grand-Prix-aux-Data-Intelligence-Awards-2013-Documation-MIS-Focus-sur-les-projets_a2055.html>`_, toutes catégories confondues, dans le cadre du salon Documation - MIS 2013. Logilab félicite les équipes de la Bibliothèque nationale de France pour ce titre récompensant un travail visionnaire de plusieurs années, et remercie la communauté `CubicWeb <http://www.cubicweb.org>`_ pour son soutien.

