
Le portail FEVIS a été lancé !
##############################


:slug: le-portail-fevis-a-ete-lance
:date: 2016/05/11 18:16:57
:tags: BlogEntry

La Fédération des Ensembles Vocaux et Instrumentaux Spécialisés (FEVIS) regroupe 130 ensembles vocaux et instrumentaux et promeut la diversité musicale. 

.. image :: https://www.logilab.fr/file/2584/raw

Cette semaine, la FEVIS a lancé son portail pour exposer son riche patrimoine : le portail `Human Music <http://www.human-music.eu/>`_.

Sur ce site, vous retrouverez 1 000 ans de musique interprétés par les meilleurs ensembles européens : enregistrements audios, captations vidéos, photos, articles, et une base de métadonnées pour vos recherches sur la musique.

Human Music a été réalisé par Logilab en s'appuyant sur le logiciel libre `CubicWeb <https://www.cubicweb.org/>`_ et les dernières avancées en HTML5 et JavaScript.

