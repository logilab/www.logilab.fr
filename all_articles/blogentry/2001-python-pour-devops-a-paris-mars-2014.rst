
Python pour DevOps à Paris (mars 2014)
######################################


:slug: python-pour-devops-a-paris-mars-2014
:date: 2014/03/06 14:16:28
:tags: BlogEntry

Logilab présentera "Utilisations avancées de Salt_: QA, supervision, Test-Driven Infrastructure" lors du prochain `atelier Python de l'AFPy à NUMA`_, le 24 mars 2014. La soirée se poursuivra par une mise en pratique, amenez votre ordinateur portable !

.. _`atelier Python de l'AFPy à NUMA`: https://www.numaparis.com/Evenements/Python-pour-DevOps-Ansible-SaltStack
.. _Salt: http://www.saltstack.org

.. image:: https://www.numaparis.com/var/sise/storage/images/evenements/python-pour-devops-ansible-saltstack/403546-1-fre-FR/Python-pour-DevOps-Ansible-SaltStack_span4.png

