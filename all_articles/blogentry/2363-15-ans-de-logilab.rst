
15 ans de Logilab
#################


:slug: 15-ans-de-logilab
:date: 2015/10/02 15:11:47
:tags: BlogEntry

Très belle soirée pour les 15 ans de Logilab : des sourires, de la bonne humeur, de la convivialité et une frise co-construite par `Jean-Pierre Bonnafous <https://twitter.com/ramuncho>`_ et les invités au fil de la soirée.

.. raw:: html

   <div>
   <a href="/file/2362/raw/frise_15ans_logilab.jpg"><img src="/file/2364/raw" width="200px"></a>
   </div>

