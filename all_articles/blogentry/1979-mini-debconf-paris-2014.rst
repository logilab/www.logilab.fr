
Mini DebConf Paris 2014
#######################


:slug: mini-debconf-paris-2014
:date: 2013/12/17 16:15:14
:tags: BlogEntry

Logilab, qui a l'habitude d'offrir le buffet des `Meetup Debian <http://www.meetup.com/Debian-France/>`_, a le plaisir de soutenir financièrement la `mini-conférence Debian <http://france.debian.net/evenements/minidebconf2014/>`__ qui aura lieu les 18 et 19 janvier 2014 à Paris. Soyez nombreux à nous y rejoindre !

.. image:: http://www.debian.org/Pics/joy_web_logo.png

