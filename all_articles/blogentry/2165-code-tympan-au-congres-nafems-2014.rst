
Code_TYMPAN au congrès Nafems 2014
##################################


:slug: code-tympan-au-congres-nafems-2014
:date: 2014/06/10 17:29:36
:tags: BlogEntry

Logilab a assisté au congrès Nafems_ les 4 et 5 juin 2014 à Paris, 
notamment pour accompagner EDF R&D lors de la présentation de Code_TYMPAN_, le seul logiciel libre de simulation de la propagation du bruit dans l'environnement.

Logilab participe au développement de Code_TYMPAN, qui est utilisé par EDF_ pour limiter les nuisances sonores de ses sites industriels et par le CEREMA_ pour prévoir l'impact qu'auraient des modifications des équipements routiers.

Pour découvrir les fonctionnalités de ce logiciel, lisez la présentation_ ou visitez la forge_.

.. image:: http://code-tympan.org/images/sampledata/fruitshop/Logo%20Code_TYMPAN_%20transparence_medium0.png

.. _CEREMA: http://www.cerema.fr/
.. _EDF: http://www.edf.com
.. _Nafems: http://www.nafems.org/2014/france/
.. _Code_TYMPAN: http://www.code-tympan.org
.. _CEREMA: http://www.cerema.fr/
.. _présentation: http://www.logilab.fr/file/2164/raw/NAFEMS2014_EDF-LOGILAB_Thomasson-Chauvat.pdf
.. _forge: https://bitbucket.org/TYMPAN/code_tympan

