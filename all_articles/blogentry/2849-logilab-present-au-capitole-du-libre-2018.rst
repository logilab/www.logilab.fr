
Logilab présent au Capitole du Libre 2018
#########################################


:slug: logilab-present-au-capitole-du-libre-2018
:date: 2018/12/05 14:40:05
:tags: BlogEntry

Logilab, comme les années précédentes, a apporté son soutien au `Capitole du Libre à Toulouse <https://2018.capitoledulibre.org/>`_
et a contribué à son programme en présentant `Déployer des applications python dans un cluster openshift <https://2018.capitoledulibre.org/programme/#deployer-des-applications-python-dans-un-cluster-2>`_ et `Retour d'expérience sur la mise en place de déploiement continu <https://2018.capitoledulibre.org/programme/#retour-dexperience-sur-la-mise-en-place-de-deploie>`_.

.. image :: https://www.logilab.fr/file/2848/raw

Ces deux présentations ont été assurées par `Philippe Pepiot <https://twitter.com/philpep_>`_ et `Arthur Lutz <https://twitter.com/arthurlutz>`_, tous les deux ingénieurs développeurs au sein de Logilab.

