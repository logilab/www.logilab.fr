
Rejoignez-nous aux RRLL 2015 jeudi 3 décembre
#############################################


:slug: rejoignez-nous-aux-rrll-2015-jeudi-3-decembre
:date: 2015/11/27 12:50:37
:tags: BlogEntry

.. image :: https://www.logilab.fr/file/2410?vid=download
   :width: 100%

Inscrivez-vous et découvrez comment Logilab aide les archives, les bibliothèques et les musées à intégrer leurs données grâce au Web 3.0.

Rendez-vous ce jeudi 3 décembre à l’Hippodrome de Toulouse, de 14h30 à 21h.

`Entrée gratuite en utilisant le code RRLL_2015 <http://www.solibre.fr/fr/actualites/rejoignez-nous-aux-rrll-2015.html>`_.

