
Découvrez la présentation de Logilab aux Rencontres Régionales du Logiciel Libre
################################################################################


:slug: decouvrez-la-presentation-de-logilab-aux-rencontres-regionales-du-logiciel-libre
:date: 2016/10/05 15:48:36
:tags: BlogEntry

**Logilab a participé à la 4ème édition des Rencontres Régionales du Logiciel Libre qui a eu lieu le 4 octobre à Toulouse.**

À cette occasion, `Sylvain Thénault <https://twitter.com/sythenault>`_ a présenté `Open Source & Open data : les bienfaits des communs <http://slides.logilab.fr/2016/RRLL_Toulouse_PosterSolutions_Logilab.pdf>`_. Découvrez sa présentation !

.. image :: https://www.logilab.fr/file/2658/raw


Vous pouvez également visualisez cette présentation sur `slideshare <http://www.slideshare.net/logilab/open-source-open-data-les-bienfaits-des-communs>`_ ou `LinkedIn <https://www.linkedin.com/company/131423>`_.

