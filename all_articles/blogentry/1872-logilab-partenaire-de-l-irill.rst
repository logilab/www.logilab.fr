
Logilab partenaire de l'IRILL
#############################


:slug: logilab-partenaire-de-l-irill
:date: 2012/11/27 22:52:32
:tags: BlogEntry

Logilab a le plaisir d'annoncer la signature de son partenariat avec le `Centre de Recherche et Innovation sur le Logiciel Libre (IRILL) <http://www.irill.org>`_, fondé à l'initiative de l'INRIA et des universités Paris 6 et Paris 7.

