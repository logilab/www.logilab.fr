
Meetup Nantes monitoring : netdata et sensu, c'est demain !
###########################################################


:slug: meetup-nantes-monitoring-netdata-et-sensu-c-est-demain
:date: 2018/01/15 14:56:28
:tags: BlogEntry

Demain, mardi 16 janvier, `Arthur Lutz <https://twitter.com/arthurlutz>`_ vous invite au meetup Nantes monitoring où netdata et sensu sont à l'ordre du jour :

• `netdata <https://my-netdata.io/>`_ pour la collecte et la visualisation de la métrologie 

.. image :: https://www.logilab.fr/file/2764/raw

• `sensu <https://sensuapp.org/>`_ pour la supervision 

.. image :: https://www.logilab.fr/file/2763/raw

Ce meet-up aura lieu de **19:00 à 22:00 au VA Solutions** situé au 3 rue du Tisserand · Saint-Herblain (5 minutes à pied de l'arrêt de tram François Mitterrand sur la ligne 1).

`INSCRIVEZ-VOUS ! <https://www.meetup.com/fr-FR/Nantes-Monitoring/events/245470310/?eventId=245470310>`_

