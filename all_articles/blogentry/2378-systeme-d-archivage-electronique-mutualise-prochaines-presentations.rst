
Système d'Archivage Électronique Mutualisé : prochaines présentations
#####################################################################


:slug: systeme-d-archivage-electronique-mutualise-prochaines-presentations
:date: 2015/11/13 18:12:09
:tags: BlogEntry

De nos jours, les institutions publiques locales doivent conjuguer efficacité et économie. Cherchant à résoudre cette équation complexe, les services d'Archives du Conseil Départemental de Gironde, de la Métropole de Bordeaux et de la Ville de Bordeaux ont choisi de s'allier pour développer et déployer un Système d'Archivage Électronique Mutualisé (SAEM) construit à partir de logiciels libres.

Venez découvrir cet outil innovant et le projet qui a permis son élaboration, en particulier :

- comment les différents acteurs ont mis en place un parrainage afin de mutualiser les expériences des partenaires et de favoriser la montée en compétence de chacun,
- comment les collectivités locales ont optimisé et partagé les coûts d'investissement,
- comment a été construite, en commun, une offre d'archivage électronique de confiance basée sur des briques logicielles libres. 

Découvrez comment vous pourriez tirer parti de cet outil et participer à son amélioration : http://saem.e-bordeaux.org/

Nos présentations allient le point de vue du client (Pascal Romain du Conseil Départemental de Gironde) et le regard technique (Sylvain Thénault de Logilab).

Retrouvez-nous :

- `Paris Open Source Summit <http://www.opensourcesummit.paris/>`_ : jeudi 19 novembre à 15h, au Docs, salle Madrid, à Paris
- `Capitole du Libre <https://2015.capitoledulibre.org/>`_ : samedi 21 novembre, à 16h30, à l'ENSEEIHT, salle A303, à Toulouse
- `Rencontres Régionales du Logiciel Libre <http://www.solibre.fr/fr/actualites/rejoignez-nous-aux-rrll-2015.html>`_ : jeudi 3 décembre, à 15h30, à l'Hippodrome de Toulouse

`Contactez-nous <https://www.logilab.fr/contact>`_ pour de plus amples informations concernant le Système d'Archivage Électronique Mutualisé.

Paris : (+33) 1 45 32 03 12
Toulouse : (+33) 5 62 17 16 42
Suivez nos actualités sur Twitter : `@logilab <https://twitter.com/logilab>`_

