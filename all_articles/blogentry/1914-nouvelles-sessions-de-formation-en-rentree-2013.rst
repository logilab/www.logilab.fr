
Nouvelles sessions de formation en rentrée 2013
###############################################


:slug: nouvelles-sessions-de-formation-en-rentree-2013
:date: 2013/07/04 15:31:15
:tags: BlogEntry

Le calendrier des formations inter-entreprises pour la rentrée 2013 se voit complété par de nombreuses formations. Du Python mais également de l'administration système ou de la création de paquets pour Debian GNU Linux, sur Paris et Toulouse.

Demandez le `programme`_ !

.. _`programme`: http://www.logilab.fr/formations

