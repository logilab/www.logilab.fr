
Logilab au Capitole du Libre les 14/15/16 novembre 2014 à Toulouse
##################################################################


:slug: logilab-au-capitole-du-libre-les-14-15-16-novembre-2014-a-toulouse
:date: 2014/10/17 10:31:05
:tags: BlogEntry

Logilab participera à la `journée pro`_ appartenant à la série `Rencontre régionale du logiciel libre`_ du CNLL_, animera un stand pendant le week-end, une `conférence sur Saltstack`_
le samedi et deux ateliers le dimanche (`improvisation
d'application sur la base de données ouverte avec CubicWeb`_ et
mise en place pratique de Saltstack).

Plus d'information sur le `site du Capitol du Libre`_.

.. image:: http://2014.capitoledulibre.org/symposion_media/static/cdl2014/img/logo-cdl.png


.. _`journée pro`: http://www.solibre.fr/fr/actualites/rejoignez-nous-aux-rrll-2014.html
.. _`Rencontre régionale du logiciel libre`: http://www.cnll.fr/news/rrll-2014-solibre/
.. _CNLL: http://www.cnll.fr
.. _`conférence sur Saltstack`: http://2014.capitoledulibre.org/schedule/presentation/89/
.. _`improvisation d'application sur la base de données ouverte avec CubicWeb`: http://2014.capitoledulibre.org/schedule/presentation/86/
.. _`site du Capitol du Libre`: http://2014.capitoledulibre.org/

