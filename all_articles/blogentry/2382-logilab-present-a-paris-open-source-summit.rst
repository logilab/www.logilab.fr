
Logilab présent à Paris Open Source Summit
##########################################


:slug: logilab-present-a-paris-open-source-summit
:date: 2015/11/17 11:54:33
:tags: BlogEntry

Né de la fusion de l'Open World Forum et de Solutions Linux, `Paris Open Source Summit <http://www.opensourcesummit.paris/>`_ aura lieu ces mercredi 18 et jeudi 19 novembre aux `Docks de Paris, à Saint-Denis <http://www.opensourcesummit.paris/Le+lieu_154_4554.html>`_. 

`Inscrivez-vous <http://www.opensourcesummit.paris/preinscription_154_204_p.html>`_ et assistez aux présentations de l'équipe Logilab :

- "Utiliser Salt pour tester son infrastructure Docker ou OpenStack", par `David Douard <https://www.logilab.fr/card/david.douard>`_, expert outils et systèmes, mercredi 18 novembre à 16h40 en salle Venise.

- "Système d'Archivage Électronique Mutualisé", par `Sylvain Thénault <https://www.logilab.fr/card/sylvain.thenault>`_, expert informatique systèmes complexes et `Pascal Romain <https://www.linkedin.com/in/pascal-romain-a4977790>`_, conseil général de la Gironde, jeudi 19 novembre, à 15h00 en salle Madrid.

