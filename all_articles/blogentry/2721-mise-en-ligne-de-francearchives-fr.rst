
Mise en ligne de FranceArchives.fr
##################################


:slug: mise-en-ligne-de-francearchives-fr
:date: 2017/07/07 22:12:55
:tags: BlogEntry

Logilab se réjouit de la mise en ligne de `FranceArchives.fr <http://francearchives.fr/>`_ le mois dernier et félicite tous ceux qui ont participé à ce beau projet qui a reçu un accueil chaleureux dans la presse spécialisée et sur les réseaux sociaux !

