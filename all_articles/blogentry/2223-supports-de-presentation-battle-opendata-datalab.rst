
Supports de présentation Battle OpenData - DataLab
##################################################


:slug: supports-de-presentation-battle-opendata-datalab
:date: 2014/12/15 15:46:55
:tags: BlogEntry

Nous avons participé à une "Battle" sur l'Open Data à l'invitation de `LiberTIC <http://libertic.wordpress.com/>`_ et `DataLab <http://www.datalab-paysdelaloire.org/>`_ (comme nous l'avions `annoncé <http://www.logilab.fr/blogentry/2208>`_). Nous avons défendu les couleurs de `CubicWeb <http://www.cubicweb.org>`_ pour la publication de données ouvertes. CubicWeb est utilisé par `la BnF <http://data.bnf.fr>`_, le `portail Open Data de la Gironde <http://www.datalocale.fr/>`_ et bien d'autres.

DataLab a publié `un court compte rendu sur la rencontre <http://www.datalab-paysdelaloire.org/Battle-Open-Data%2C-quelle-plateforme-pour-quel-projet-%3F>`_, où vous pourrez trouver les présentations de chacun des participants.

Notre présentation `en pdf <http://www.logilab.fr/file/2221/raw/Battle%20Opendata.pdf>`_ (ou une copie `sur slideshare <http://www.slideshare.net/Datalab_PDL/battle-opendata-logilab-42673173>`_).

