
Découvrons les nouveautés de PostgreSQL 9.5 à Toulouse
######################################################


:slug: decouvrons-les-nouveautes-de-postgresql-9-5-a-toulouse
:date: 2016/02/08 17:17:58
:tags: BlogEntry

Rejoignez-nous au prochain meet-up PostgreSQL que nous co-organisons chez Digital Place, à Toulouse et découvrez sa nouvelle version 9.5, ses nouveautés et ses changements.



`Rendez-vous mardi 23 février à midi chez Digital Place. <http://www.meetup.com/fr-FR/PostgreSQL-User-Group-Toulouse/events/228604600/>`_
 
.. image :: https://www.logilab.fr/file/2504?vid=download

