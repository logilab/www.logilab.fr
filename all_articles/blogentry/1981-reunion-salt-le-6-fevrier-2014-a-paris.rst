
Réunion Salt le 6 février 2014 à Paris
######################################


:slug: reunion-salt-le-6-fevrier-2014-a-paris
:date: 2013/12/17 16:34:04
:tags: BlogEntry

Logilab organisera le 6 février 2014 à Paris à partir de 18h, dans les locaux de son partenaire l'IRILL_, la deuxième réunion des utilisateurs et développeurs de Salt_ en France. Salt est un environnement d'exécution distribué et asynchrone, écrit en Python, qui se positionne comme le couteau suisse de la gestion d'infrastructure.

Logilab a choisi Salt pour la gestion de sa propre infrastructure (interne multi-site et `calcul / simulagora <http://www.simulagora.com>`_) et propose du `conseil et de la formation <http://www.logilab.fr/training/1942>`_ pour le maîtriser rapidement.

.. _IRILL: http://www.irill.org/
.. _Salt: http://www.saltstack.com/community/

L'addresse de l'IRILL est 23 avenue d'Italie, 75013 Paris.

.. image:: http://www.logilab.fr/file/2163/raw/saltstack_logo.png
   :width: 400px

