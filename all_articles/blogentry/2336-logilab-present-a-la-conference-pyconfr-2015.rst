
Logilab présent à la conférence PyConFr 2015
############################################


:slug: logilab-present-a-la-conference-pyconfr-2015
:date: 2015/08/26 11:29:08
:tags: BlogEntry

Assistez aux présentations des ingénieurs développeurs de Logilab à l'occasion de la conférence `Pycon <http://www.pycon.fr/2015/>`_ qui aura lieu du 17 au 20 octobre à Pau :

- Importer des données en `Python <http://www.python.org>`_ avec `CubicWeb <https://www.cubicweb.org/>`_ 3.21, par Yann Vote
- Utiliser `Salt <http://saltstack.com/community/>`_ pour tester son infrastructure sur OpenStack ou Docker avant la mise en production, par Arthur Lutz
- Marre de faire du C++ sur une Arduino ? Faites du Python avec MicroPython sur une PyBoard, également par Arthur Lutz

À noter que Christophe de Vienne d'`Unlish <http://www.unlish.fr>`_ parlera de l'insertion de CubicWeb dans l'environnement `Pyramid <http://www.pylonsproject.org/projects/pyramid/about>`_\ .

