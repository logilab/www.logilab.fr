
data.bnf.fr - épisode II
########################


:slug: data-bnf-fr-episode-ii
:date: 2012/09/12 15:31:19
:tags: BlogEntry

Logilab a `remporté l'appel d'offre`__ lancé par la Bibliothèque nationale de France pour l'évolution de son catalogue mis en ligne sur le web des données: `data.bnf.fr`_.

__ https://marchespublics.bnf.fr/dmp/wdepdmp.nsf/wAll/863BDF55B8C45631C1257A68002C8E67?openDocument

.. _`data.bnf.fr`: http://data.bnf.fr

