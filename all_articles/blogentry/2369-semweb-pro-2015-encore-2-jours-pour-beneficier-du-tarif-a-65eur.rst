
SemWeb.Pro 2015 : Encore 2 jours pour bénéficier du tarif à 65€
###############################################################


:slug: semweb-pro-2015-encore-2-jours-pour-beneficier-du-tarif-a-65eur
:date: 2015/10/20 18:54:36
:tags: BlogEntry

****jeudi 5 novembre au FIAP Jean Monnet, à Paris****

Inscrivez-vous avant le jeudi 22 octobre pour bénéficier du tarif à 65€

Consultez le programme et participez à la conférence dédiée au web sémantique :

- 08h30    Accueil
- 09h30    Tutoriel d'introduction au web sémantique, par Nicolas Chauvat - Logilab
- 10h15    Retour MOOC web sémantique, par Fabien Gandon - INRIA
- 10h30    Pause-café
- 11h00    Feuille de route 3.0 du Ministère de la Culture, par Bertrand Sajus - Ministère de la Culture
- 11h15    Libre Théâtre, plateforme facilitant l'accès gratuit aux textes de théâtre français libres de droit, par Ruth Martinez - Association Libre Théâtre
- 11h30    Utilisation de data.bnf.fr pour alimenter WikiData, par Raphaëlle Lapôtre et Benoît Deshayes - BnF
- 11h45    Biblissima : une nouvelles bibliothèque des bibliothèques du Moyen-Âge et de la Renaissance, par Stefanie Gehrke - Biblissima
- 12h00    Questions
- 12h20    Déjeuner
- 13h45    Keynote : The Next 10 Years of Success - reloaded, par Phil Archer - W3C
- 14h30    Gestion de serveurs avec une plateforme sémantique, par Frédéric Hay - Straton IT
- 14h45    ELI / ECLI : des identifiants pour le croisement des sources ouvertes du droit, par Jean Delahousse - DILA
- 15h15    Évolution d'un système de publication de données techniques automobiles, modélisées en RDF, par François-Paul Servant - Renault
- 15h30    Pause-café
- 16h15    Open Data française à l'heure du Linked Data, par Colin Maudry
- 16h30    Plugin SMILK : données liées et traitement de la langue pour améliorer la navigation Web, par Farhad Nooralahzadeh - INRIA
- 16h45    Interroger efficacement des bases de données relationnelles avec SPARQL et Ontop, par Benjamin Cogrel - Université de Bolzano
- 17h00    Sparklis : exploration et interrogation de points d'accès SPARQL par interaction et langue naturelle, par Sébastien Ferré - Université de Rennes 1
- 17h15    Questions

Pour plus d'informations, contactez-nous : contact@semweb.pro

En attendant de vous recevoir nombreux,
Logilab.

-----------

Pour les éditions précédentes consultez http://semweb.pro/conference/

Suivez les nouvelles sur Twitter @semwebpro <https://twitter.com/semwebpro> #semwebpro

