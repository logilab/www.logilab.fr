
SemWeb.Pro 2012
###############


:slug: semweb-pro-2012
:date: 2012/04/02 12:29:02
:tags: BlogEntry

Les deuxièmes journées professionnelles du Web Sémantique auront lieu les 2 et 3 mai 2012 à Paris. Une journée de conférence et une journée de tutoriels pour faire se rencontrer les professionnels du Web Sémantique, à savoir les membres de la communauté scientifique et les industriels désireux de mettre en oeuvre ces nouvelles techniques. Inscription sur `SemWeb.Pro <http://www.semweb.pro/>`_.

