
Nous recrutons !
################


:slug: nous-recrutons
:date: 2017/09/26 10:47:57
:tags: BlogEntry

Poursuivant notre développement en 2017, nous cherchons des ingénieurs afin de renforcer nos équipes de R&D :

   * `CDI - Directeur de projets agiles </ddp1>`_
   * `CDI - Développement web (client / *Front-end*) </inge3>`_
   * `CDI - Développement informatique et web sémantique </inge2>`_ 



.. image :: https://www.logilab.fr/file/2618/raw

