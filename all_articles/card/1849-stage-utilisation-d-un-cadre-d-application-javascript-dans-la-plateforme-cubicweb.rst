
Stage - Utilisation d'un cadre d'application Javascript dans la plateforme CubicWeb
###################################################################################


:slug: stage-utilisation-d-un-cadre-d-application-javascript-dans-la-plateforme-cubicweb
:date: 2012/11/12 17:15:33
:tags: Card

Contexte
========

CubicWeb_ est une plateforme logicielle libre permettant de développer
rapidement des applications de gestion de données. Elle s'appuie
fortement sur le modèle métier des informations gérées pour proposer
une approche originale laissant à l'utilisateur final toute latitude
pour naviguer dans le graphe des informations, sélectionner des
données et choisir la vue qui lui permettra de les visualiser. De son
côté, le développeur dispose de fonctionnalités automatisant, à partir
du modèle métier, nombre de tâches habituelles (gestion de
la base relationnelle sous-jacente, génération d'une interface Web par
défaut, migration entre versions successives, etc.)

.. _CubicWeb: http://www.cubicweb.org/

Problématique
=============

Aujourd'hui, les applications Web deviennent toujours plus dynamiques
et ergonomiques, devenant même des choix alternatifs crédibles aux IHM
lourdes. La mise en œuvre de ce type d'applications nécessite
toutefois le recours à de nombreuses fonctionnalités dynamiques côté
client, dans le navigateur. Ces fonctionnalités sont donc codées en
Javascript. Afin de simplifier la mise en œuvre des applications Web,
différents cadres d'application (*framework*) Javascript ont vu le
jour ; parmi ceux-ci, les cadres d'application MVC (*Model View
Controller*) qui semblent les plus prometteurs sont backbone.js, ember.js,
javascriptmvc, etc.

L'objectif du stage est d'étudier et tester l'apport des cadres
d'application MVC Javascript à la plateforme CubicWeb.

Rôle
====

Intégré à l'équipe de recherche et développement "Web Sémantique" de
Logilab, sous la tutelle d'un ingénieur spécialiste de la
programmation Web et de la plateforme CubicWeb, vous l'assisterez dans
son travail quotidien et serez amené à effectuer tout ou partie
des travaux suivants, en collaboration avec l'équipe :

* étudier et comparer les principaux cadres d'applications MVC Javascript,

* réaliser une application CubicWeb en s'appuyant sur un de ces cadres
  d'application.

Passionné par l'informatique, appréciant d'interagir avec une équipe
dynamique, aimant développer de façon agile tout en restant ordonné,
vous savez que la qualité d'un travail s'apprécie surtout dans sa
concision. Ce stage est donc fait pour vous ! Envoyez-nous votre
candidature à l'adresse personnel@logilab.fr

Compétences attendues
=====================

* Bonnes connaissances de la programmation Web et du protocole HTTP,
* Bonnes connaissances pratiques en programmation Javascript,
* Connaissances honnêtes de l'écosystème autour de Javascript,
* Anglais technique,
* La connaissance du langage Python_ sera appréciée.

.. _Python: http://www.python.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

