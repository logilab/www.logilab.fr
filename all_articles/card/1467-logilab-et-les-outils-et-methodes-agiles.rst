
Logilab et les outils et méthodes agiles
########################################


:slug: logilab-et-les-outils-et-methodes-agiles
:date: 2011/12/01 15:52:37
:tags: Card

Dès sa création, début 2000, Logilab s'est organisée sur les principes des méthodes agiles et des projets de Logiciel Libre, dont ses fondateurs avaient déjà l'expérience, en les appliquant au développement logiciel et à la gestion de projet.

Après plus de dix ans de pratique quotidienne de ces méthodes, appliquées à la fois pour les développements internes et les projets menés pour des clients, Logilab est à même de partager cette expérience au travers de missions de formation, de conseil et d'accompagnement.

Qu'il s'agisse d'améliorer la gestion des projets par des prévisions plus justes et des réactions plus rapides aux changements, ou bien d'accroître la vitesse de développement d'une équipe tout en réduisant la fréquence d'apparition des défauts, les méthodes agiles sont une approche à ne pas négliger.

