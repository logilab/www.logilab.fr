
Partenariats noués par Logilab
##############################


:slug: partenariats-noues-par-logilab
:date: 2011/12/01 15:52:37
:tags: Card

À ce jour, Logilab a noué les partenariats suivants :
      
* `Systematic Paris Région <http://www.systematic-paris-region.org/>`_ est le pôle de compétitivité de la région Ile-de-France, dédié aux logiciels et systèmes complexes.


.. image:: http://www.logilab.fr/file/1410?vid=download


* `APRIL <http://www.april.org/>`_  est l'Association pour la Promotion et la Recherche en Informatique Libre. Depuis 1996 c'est un acteur majeur de la démocratisation et de la diffusion du logiciel libre et des standards ouverts auprès du grand public, des professionnels et des institutions dans l'espace francophone. Elle veille aussi, dans l'ère numérique, à sensibiliser l'opinion sur les dangers d'une appropriation exclusive de l'information et du savoir par des intérêts privés.

.. image:: http://www.logilab.fr/file/1774?vid=download


* `EuroPython Conference <http://www.europython.org/>`_ est la conférence européenne dédiée au langage Python et à ses applications. Logilab a participé à l'organisation de cet événement annuel, et en particulier de la session "Python et les applications scientifiques".

* **Kiddanet** est un projet européen qui visait à déployer un portail permettant aux parents et aux éducateurs de laisser les enfants de naviguer sur Internet sous le contrôle d'un système de filtrage multi-agents. Logilab fut l'un des sept membres du consortium auquel avait été confiée la réalisation de ce projet.

* **ASWAD** est un projet européen qui visait à déployer un environnement de travail collaboratif intégrant des agents intelligents pour faciliter le traitement et l'automatisation de certaines tâches. Ce projet reposait sur Plone et Narval, l'environnement de déploiement d'assistants développé par Logilab. Logilab fut membre du consortium auquel avait été confiée la réalisation de ce projet.

* `PyPy <http://www.pypy.org/>`_ a pour objectif de réécrire l'interpréteur Python en Python afin d'obtenir un langage plus souple pouvant bénéficier des avancées techniques disponibles dans certains projets connexes. Il permettra également de fabriquer des interpréteurs Python minimaux personnalisables pouvant être aisément embarqués. Logilab fut membre du consortium auquel avait été confiée la réalisation du projet de recherche européen qui a abouti à la première version de PyPy.

