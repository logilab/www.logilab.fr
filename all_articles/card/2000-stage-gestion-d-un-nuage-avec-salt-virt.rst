
Stage  - Gestion d'un nuage avec Salt-virt
##########################################


:slug: stage-gestion-d-un-nuage-avec-salt-virt
:date: 2014/02/21 15:46:00
:tags: Card

Contexte
========

L'arrivée des nuages de serveurs (*cloud computing*) a remis fortement
en cause la façon d'aborder l'informatique et plus particulièrement le
calcul scientifique. Logilab administre son propre nuage OpenStack,
sur lequel sont executés des algorithmes de fouille des données et des
codes de simulation numérique.

Par ailleurs, Salt_ est un environnement d'exécution distribué et
asynchrone, qui se positionne comme le *couteau suisse* de la gestion
d'infrastructure. Qu'il s'agisse d'exécuter une commande sur plusieurs
machines, de définir puis appliquer une configuration système, de
récolter les mesures faites par des sondes, de lancer des machines
virtuelles ou de gérer un "cloud": Salt a une solution.

Logilab a choisi Salt pour sa rapidité d'exécution, sa facilité d'accès, sa
versatilité et son extensibilité qui en font un outil efficace, bien qu'il
demande du temps pour être pleinement maîtrisé.

Problématique
=============

L'objectif du stage sera d'aider à l'administration du nuage de
Logilab en évaluant et en proposant une solution alternative à
OpenStack pour fournir les services d'infrastructure élastique en
s'appuyant sur Salt_ et salt-virt_ en particulier. Ce travail
demandera probablement de participer au développement de Salt_
lui-même.

.. _Salt: http://www.saltstack.org/
.. _salt-virt: http://docs.saltstack.com/topics/virt/

Rôle
====

Intégré à l'équipe de recherche et développement "Outils et systèmes" de
Logilab, sous assisterez un ingénieur expérimenté dans son travail quotidien et
pourrez être amené à effectuer tout ou partie des travaux suivants, en
collaboration avec l'équipe :

* mise en œuvre d'OpenStack et Salt-virt sur la base de Debian_,

* développement des outils et connecteurs manquants, contribution à ces projets
  libres.

.. _Debian: http://www.debian.org/

Compétences attendues
=====================

* Bonnes connaissances pratiques en programmation objet,
* Connaissance du langage de programmation Python_
* Connaissance de l'administration système sous Linux avec Debian_,
* Anglais technique,
* Les connaissances de la virtualisation de serveur seront appréciées
  (idéalement SaltStack_ et OpenStack_).

.. _OpenStack: http://www.openstack.org/
.. _Python: http://www.python.org/
.. _SaltStack: http://www.saltstack.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

Dans un souci de maintien de la parité, les candidatures féminines
seront examinées avec attention.

