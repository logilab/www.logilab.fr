
Stage Ergonomie Application Web
###############################


:slug: stage-ergonomie-application-web
:date: 2015/05/07 12:39:06
:tags: Card

Contexte
========

CubicWeb_ est une plateforme logicielle libre permettant de développer
rapidement des applications de gestion de données. Elle s'appuie
fortement sur le modèle métier des informations gérées pour proposer
une approche originale laissant à l'utilisateur final toute latitude
pour naviguer dans le graphe des informations, sélectionner des
données et choisir la vue qui lui permettra de les visualiser. De son
côté, le développeur dispose de fonctionnalités automatisant, à partir
du modèle métier, nombre de tâches habituelles (gestion de
la base relationnelle sous-jacente, génération d'une interface Web par
défaut, migration entre versions successives, etc.)

.. _CubicWeb: http://www.cubicweb.org/


Rôle
====

Intégré à l'équipe de recherche et développement de Logilab, sous la tutelle d'un ingénieur spécialiste de l'architecture Web et de la plateforme CubicWeb, vous lui apporterez votre soutien sur les aspects concernant l'ergonomie et l'expérience utilisateur (UX) et serez amené à effectuer tout ou partie des travaux suivants :

- se familiariser avec la plateforme CubicWeb,
- analyser une ou plusieurs applications CubicWeb existantes et recueillir les retours de leurs utilisateurs,
- rédiger une série de recommandations concrètes pour améliorer l'ergonomie des applications étudiées,
- implémenter tout ou partie de ces recommandations.

Technologies utilisées :

- HTML5/JavaScript
- Python
- `CubicWeb <http://www.cubicweb.org/>`_
- Debian
- Mercurial

.. _Debian: http://www.debian/org/
.. _Freeswitch: http://www.freeswitch.org/
.. _ejabberd: http://www.ejabberd.im/
.. _Postfix: http://www.postfix.org/
.. _Jabber: http://www.jabber.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur) ou étudiant BAC+4/+5 en informatique.

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

