
CDI - Direct(eur/rice) de projets agiles
########################################


:slug: cdi-direct-eur-rice-de-projets-agiles
:date: 2017/07/07 21:08:01
:tags: Card

Vous avez fait vos armes dans le développement logiciel avant d'animer et
coordonner des projets.

Vous souhaitez poursuivre sur cette voie et mener une ou plusieurs équipes
de développement vers le succès dans la réalisation de projets ambitieux en
vous appuyant sur leurs solides compétences techniques.

Vous appréciez l'efficacité que procurent les méthodes agiles et savez
d'expérience que la liberté et la créativité sont les fruits de l'exigence et de
la discipline.

Rejoignez notre équipe !

Mission
-------

En charge de projets d'informatique scientifique ou de gestion des connaissances,
vous organiserez et coordonnerez le travail des membres de l'équipe pour qu'émerge le meilleur compromis entre les attentes des clients et
utilisateurs, les contraintes de rentabilité et les possibilités techniques.

Profil
------

Ingénieur ou équivalent - BAC+5 ou supérieur.

Expérience
----------

5 ans et plus, dont plusieurs années de développement et une expérience de la
gestion de projet.

Compétences / Connaissances souhaitées
--------------------------------------

* Direction d'un projet, animation des réunions, recette des livrables, etc. en suivant les principes des méthodes agiles d'organisation (SCRUM, XP, etc.)

* Encadrement d'une équipe de développement

* Architecture de système d'information (Bases de données, client-serveur, réseau, objets distribués, etc.)

* Aisance pour rédiger des synthèses et présenter en public, y compris en
  anglais.

* Conception fonctionnelle et technique des logiciels.

* Expérience du développement logiciel.

* Habitude du logiciel libre.

Durée
-----
Contrat à Durée Indéterminée.

Lieu
----
Paris (75013).

Rémunération
--------------

Rémunération selon le profil.

Candidatures
-------------

Envoyez votre candidature (CV + lettre de motivation, format PDF ou HTML) par
courrier électronique à personnel@logilab.fr.

