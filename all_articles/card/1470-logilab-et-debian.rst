
Logilab et Debian
#################


:slug: logilab-et-debian
:date: 2011/12/01 15:52:37
:tags: Card

Logilab fournit pour tous les Logiciels Libres qu'elle développe des paquets permettant de les installer aisément sur une distribution `Debian    GNU/Linux <http://www.debian.org/>`_ . Cette distribution est utilisée sur tout le parc informatique de Logilab. Elle a la particulatité d'être non commerciale et    constituée exclusivement de Logiciels Libres, contrairement à d'autres distributions du système d'exploitation Linux.

Pour en savoir plus sur Debian, reportez-vous à notre article intitulé `Pourquoi choisir Debian pour déployer Linux dans son entreprise ? </pourquoi-debian>`_.

.. image :: http://www.debian.org/logos/openlogo-100.jpg

