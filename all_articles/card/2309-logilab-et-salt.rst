
Logilab et Salt
###############


:slug: logilab-et-salt
:date: 2015/04/23 17:36:48
:tags: Card

`Salt <http://saltstack.com/community/>`_ est un environnement libre d'exécution distribué et asynchrone, qui permet de gérer la configuration et l'administration des nuages et d'automatiser les tâches des équipes DevOps. 

Qu'il s'agisse d'exécuter une commande sur plusieurs machines, d'appliquer une configuration système, de récolter les mesures faites par des sondes, de lancer des machines virtuelles ou de gérer un "cloud", Salt a une solution.

Utilisé par des réseaux sociaux tels que Dailymotion et LinkedIn, ainsi que par les acteurs majeurs du cloud, Salt fournit un bus de communication et un client d'empreinte mémoire réduite, qui fonctionnent sur toutes les plateformes (Unix, MacOS, Windows, embarqué, etc.). Il permet de décrire complètement la configuration de machines et d'applications avec un ensemble de fichiers yaml et de scripts Python qui deviennent le "code source" du système d'information.

Grâce à Salt, le système d'information peut être développé et maintenu comme n'importe quelle application logicielle (réutilisation de composants, code source contrôlé, travail collaboratif, accès à des données ou services partagés) et il devient plus simple d'appliquer les principes des méthodes agiles (Test Driven Development/Infrastructure, développement itératif, intégration continue, etc).

Nous disposons d'une équipe certifiée SaltStack pour assurer le support technique, la formation, ainsi que la certification au logiciel Salt.

Pour en apprendre plus, rejoignez la communauté `salt-fr <http://salt-fr.afpy.org/>`_ ou inscrivez-vous à la prochaine session de notre `formation salt <http://www.logilab.fr/formations/salt>`_.

