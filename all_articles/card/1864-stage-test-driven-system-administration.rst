
Stage  - Test-Driven System Administration
##########################################


:slug: stage-test-driven-system-administration
:date: 2012/11/13 11:12:28
:tags: Card

Contexte
========

L'arrivée de la virtualisation et des nuages de serveurs (*cloud computing*) a
complexifié l'administration système. En effet, si de nombreux outils existent
pour automatiser la configuration et le déploiement de serveurs ou pour
surveiller des systèmes existants, il y a beaucoup de redondance dans les
informations et nous ne connaissons pas d'exemple fusionnant les deux fonctions.

Problématique
=============

L'objectif de ce stage est de chercher à conjuguer la méthode du
`Test-Driven Development`_ avec les besoins et contraintes de
l'administration système. Pour ce faire, on tentera d'aboutir à une
description logique d'un système informatique qui sera utilisée comme
entrée pour le logiciel chargé du déploiement (de préférence à l'aide
de OpenStack_ et de SaltStack_) et à une application de surveillance
du réseau et des services hébergés via Shinken_ (un avatar de
Nagios_).

.. _`Test-Driven Development`: http://fr.wikipedia.org/wiki/Test_Driven_Development
.. _OpenStack: http://www.openstack.org/
.. _SaltStack: http://www.saltstack.org/
.. _Shinken: http://shinken-monitoring.org/
.. _Nagios: http://nagios.org/

Rôle
====

Intégré à l'équipe de recherche et développement "Outils et systèmes" de
Logilab, vous assisterez un ingénieur expérimenté dans son travail quotidien et
pourrez être amené à effectuer tout ou partie des travaux suivants, en
collaboration avec l'équipe :

* décrire un système informatique de manière à alimenter les applications de
  déploiement et de surveillance,

* développer les composants nécessaires à leur mise en œuvre.

Compétences attendues
=====================

* Bonnes connaissances pratiques en programmation objet,
* Connaissance de l'administration système sous Linux avec Debian_,
* Anglais technique,
* Les connaissances du langage Python_ et de la virtualisation de serveur
  seront appréciées
* Idéalement, connaissances de SaltStack_, OpenStack_ et Nagios_.

.. _Python: http://www.python.org/
.. _Debian: http://www.debian.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

