
Communication
#############


:slug: communication
:date: 2011/12/01 15:52:37
:tags: Card

Conférences et salons
=====================

.. class:: card-event

SemWeb.Pro 2011
---------------
du 2011-01-17 au 2011-01-18 à Paris
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Les premières journées professionnelles du Web Sémantique auront lieu les 17 et 18 janvier 2011 à Paris. Une journée de conférence et une journée de tutoriels pour faire se rencontrer les professionnels du Web Sémantique, à savoir les membres de la communauté scientifique et les industriels désireux de mettre en oeuvre ces nouvelles techniques. Inscription sur SemWeb.Pro.

Publications externes
=====================

Ci-dessous, une liste d'articles publiés par des membres de Logilab dans des revues ou lors de conférences :

- `Programmation par contrats et qualité logicielle <http://www.python.org/>`_  par **Nicolas Chauvat**, *paru dans Développeur Référence* v2.03 - 9 novembre 2001.
- `Python pour les applications scientifiques - exemple de l'Onera avec pyCGNS <http://www.python.org/>`_ par **Nicolas Chauvat et Marc Poinot**, *actes du MICAD 2003*, avril 2003.

Articles disponibles sur ce site
================================

* `Pourquoi choisir Debian pour déployer Linux dans son entreprise ? </pourquoi-debian>`_.
* `De l'impact du Logiciel Libre sur l'industrie informatique </impact-logiciel-libre>`_.

