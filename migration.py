# coding: utf-8

import os
import requests
from cwclientlib import cwproxy
from slugify import slugify
from datetime import datetime
from constants import (
    template_rest, template_html, templates, article_path,
    redirect_string_one, redirect_string_two
)



client = cwproxy.CWProxy('https://www.logilab.fr/')
blog_keys = ['eid', 'category', 'title',
             'content', 'creation_date', 'content_format']

query = ("Any BE, CAT, BET, CONT, DAT, FMT "
"WHERE X is Blog, X title CAT, "
"BE entry_of X, "
"BE content CONT, BE content_format FMT, BE creation_date DAT, BE title BET")

resp = client.rql(query)
blogentries = resp.json()


#  Article extraction + write of redirects


for be in blogentries:
    blogentry = dict(zip(blog_keys, be))

    template = templates[blogentry['content_format']]
    blogentry['underline'] = '#' * len(blogentry['title'])
    blogentry['etype'] = "BlogEntry"
    appendix = blogentry['content_format'].split('/')[1]
    extension = 'rst' if appendix == 'rest' else appendix
    creation_date = datetime.strptime(blogentry['creation_date'], '%Y/%m/%d %H:%M:%S')

    if blogentry['content']:
        blogentry['content'] = blogentry['content'].replace(':rql:', 'rql')

    if blogentry['title']:
        blogentry['slug'] = slugify(blogentry['title']).lower()
    else:
        blogentry['slug'] = blogentry['eid']


    article = article_path.format(blogentry['category'].lower(),
                                  blogentry['eid'],
                                  blogentry['slug'],
                                  extension)

    redirect1 = redirect_string_one.format(etype=blogentry['etype'],
                                           eid=blogentry['eid'],
                                           year=creation_date.year,
                                           month=creation_date.month,
                                           slug=blogentry['slug'])

    redirect2 = redirect_string_two.format(eid=blogentry['eid'],
                                           year=creation_date.year,
                                           month=creation_date.month,
                                           slug=blogentry['slug'])

    if not os.path.exists(os.path.dirname(article)):
        try:
            os.makedirs(os.path.dirname(article))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    with open(article, 'w+') as f:
        f.write(template.format(**blogentry))

    with open('redirects', 'a') as f:
        f.write(redirect1)
        f.write(redirect2)
